const axios = require("axios");
const {
  ACCESS_TOKEN,
  SHOPIFY_ADMIN_URL,
  INGRAM_INVENTORY_API,
  IM_CUSTOMER_NUMBER,
  IM_COUNTRY_CODE,
  IM_CORRELATION_ID,
} = require("./constants");

module.exports = {
  async getVariantInventory(variantSku, access_token) {
    let data = JSON.stringify({
      products: [
        {
          ingramPartNumber: variantSku,
        },
      ],
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: INGRAM_INVENTORY_API,
      headers: {
        "IM-CustomerNumber": IM_CUSTOMER_NUMBER,
        "IM-CountryCode": IM_COUNTRY_CODE,
        "IM-CorrelationID": IM_CORRELATION_ID,
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
      data: data,
    };

    return await axios
      .request(config)
      .then((response) => {
        //console.log(response.data);
        let totalAvailability = "";
        if (response.data[0].ingramPartNumber != "") {
          totalAvailability = response.data[0].availability.totalAvailability;
        }
        return totalAvailability;
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async getLocationData(name) {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: SHOPIFY_ADMIN_URL + "/api/2024-01/locations.json",
      headers: {
        "X-Shopify-Access-Token": ACCESS_TOKEN,
      },
    };

    return await axios
      .request(config)
      .then(async (response) => {
        let locations = response.data.locations;
        if (locations != undefined) {
          result = locations.filter((location) => location.name == name);
          return result[0].id;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async updateVariantInventory(sku, qty, inventory_item_id, location_id) {
    let data = JSON.stringify({
      location_id: location_id,
      inventory_item_id: inventory_item_id,
      available: qty,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: SHOPIFY_ADMIN_URL + "/api/2024-01/inventory_levels/set.json",
      headers: {
        "X-Shopify-Access-Token": ACCESS_TOKEN,
        "Content-Type": "application/json",
      },
      data: data,
    };

    return await axios
      .request(config)
      .then(() => {
        console.log("Updated for " + sku + " With Qty " + qty);
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
